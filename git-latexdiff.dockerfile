FROM perl:5.34

RUN apt-get update && \
    apt-get install -y -qq \
      git nano curl asciidoc latexdiff texlive-full\
  && apt-get upgrade -y -qq \
  && rm -rf /var/lib/apt/lists/*


RUN apt-get install -y -qq \
      texlive-publishers \
  && rm -rf /var/lib/apt/lists/*


WORKDIR /usr/src/app


RUN git clone https://gitlab.com/git-latexdiff/git-latexdiff.git

WORKDIR git-latexdiff

RUN make install

RUN mkdir /usr/repo

WORKDIR /usr/repo


ENTRYPOINT [ "git", "latexdiff" ]
CMD [ "--help-examples" ]

